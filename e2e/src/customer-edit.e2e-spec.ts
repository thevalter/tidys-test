import { browser, logging, by, element, protractor } from 'protractor';

describe('Customers new route', () => {
  beforeEach(() => {
    browser.get('/customers');
  });

  it('Should edit a customer', () => {
    const EC = protractor.ExpectedConditions;

    browser.wait(EC.presenceOf(element(by.css('.edit-link')))).then(() => {
      element(by.css('.edit-link')).getAttribute('ng-reflect-router-link').then((attr) => {
        const customerId = attr.split(',')[1];
        browser.get(`/customers/${customerId}/edit`);

        browser.ignoreSynchronization = true;
        browser.wait(EC.presenceOf(element(by.css('[placeholder=Phone]')))).then(() => {
          element(by.css('[placeholder=Phone]')).clear();
          element(by.css('[placeholder=Phone]')).sendKeys('12345678910');

          element(by.css('[type=submit]')).click().then(() => {
            browser.wait(EC.presenceOf(element(by.css('.swal2-container')))).then(() => {
              expect(element(by.css('.swal2-container')).getText())
                .toContain('Customer edited with success!');

              browser.ignoreSynchronization = false;
            });
          });
        });
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
