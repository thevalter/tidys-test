import { AppPage } from './app.po';
import { browser, logging, by, element, protractor } from 'protractor';

describe('Customers route', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.get('/customers');
  });

  it('should have a inputs and buttons', () => {
    element(by.css('.create-button')).click();
    const inputsCount = element.all(by.css('input')).count();
    const buttonsCount = element.all(by.css('button')).count();

    expect(inputsCount).toEqual(7);
    expect(buttonsCount).toEqual(2);
  });

  it('should remove a customer', () => {
    browser.ignoreSynchronization = true;
    const EC = protractor.ExpectedConditions;

    browser.wait(EC.presenceOf(element(by.css('.remove-link')))).then(() => {
      element.all(by.css('.remove-link')).last().click().then(() => {
        browser.wait(EC.presenceOf(element(by.css('.swal2-container')))).then(() => {
          expect(element(by.css('.swal2-container')).getText()).toContain('Customer deleted with success!');
          browser.ignoreSynchronization = false;
        });
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
