import { AppPage } from './app.po';
import { browser, logging, by, element } from 'protractor';

describe('Base url route', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Thanks a lot for the opportunity all code was made by Valter Barros');
  });

  it('should redirect to customers list view', () => {
    element(by.css('[href="/customers"]')).click();
    const tableElement = element.all(by.css('table'));

    expect(tableElement.count()).toEqual(1);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
