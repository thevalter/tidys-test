import { browser, logging, by, element, protractor } from 'protractor';

describe('Customers new route', () => {
  beforeEach(() => {
    browser.get('/customers/new');
  });

  it('Should create a new customer', () => {
    element(by.css('[placeholder=Name]')).sendKeys('Marcus');
    element(by.css('[placeholder=Email]')).sendKeys('marcus@tidy.com');
    element(by.css('[placeholder=Phone]')).sendKeys('5555555');
    element(by.css('[placeholder=Address]')).sendKeys('street A');
    element(by.css('[placeholder=City]')).sendKeys('Los Angeles');
    element(by.css('[placeholder=State]')).sendKeys('California');
    element(by.css('[placeholder=Zipcode]')).sendKeys('9897897897');

    const tableElement = element(by.css('table'));

    const EC = protractor.ExpectedConditions;

    browser.ignoreSynchronization = true;
    element(by.css('[type=submit]')).click().then(() => {
      browser.wait(EC.presenceOf(tableElement)).then(() => {
        expect(tableElement.isDisplayed()).toBe(true);
        browser.ignoreSynchronization = false;
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
