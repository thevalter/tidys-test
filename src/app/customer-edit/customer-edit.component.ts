import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService, Customer } from '../customer.service';
import { tap } from 'rxjs/operators';
import customerFormBuilder from '../builders/customer_form_builder';
import { Router } from '@angular/router';
import SwalBuilder from '../builders/swal_builder';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {
  customerForm;
  customers;
  customerId: string;
  processingRequest = false;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private router: Router
  ) {
    this.customerForm = customerFormBuilder.build();

    this.route.paramMap.subscribe((params) => {
      this.customerId = params.get('customerId');
    });

    this.customers = this.customerService.getCustomers().pipe(
      tap((customers: Customer[]) => {
        const customer = this.customerService.filterCustomerById(this.customerId, customers);
        this.customerForm.patchValue(customer);
      })
    );
  }

  ngOnInit() {}

  onSubmit() {
    if (this.customerForm.valid) {
      this.processingRequest = true;
      this.customerService.updateCustomer(this.customerId, this.customerForm.value, this.redirectToListAndEnableButton.bind(this));
    }
  }

  redirectToListAndEnableButton() {
    this.router.navigate(['/customers']);
    this.processingRequest = false;

    SwalBuilder.build('Customer edited with success!');
  }
}
