import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import SwalBuilder from '../builders/swal_builder';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customers;
  displayedColumns: string[] = ['name', 'email', 'phone', 'address', 'actions'];
  processingRequest = false;

  constructor(
    private router: Router,
    private customerService: CustomerService
  ) {
    this.managerCustomerData();
  }

  ngOnInit() {
  }

  onDeleteClick(customerId: number) {
    this.processingRequest = true;
    this.customerService.deleteCustomer(customerId, this.updateCustomersList.bind(this));
  }

  managerCustomerData() {
    this.customers = this.customerService.getCustomers();
  }

  updateCustomersList() {
    this.managerCustomerData();
    this.processingRequest = false;

    SwalBuilder.build('Customer deleted with success!');
  }

  onAddCustomerClick() {
    this.router.navigate(['/customers/new']);
  }
}
