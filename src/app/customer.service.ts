import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'https://tidy-api-test.herokuapp.com:443';

export interface Customer {
  id: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  zipcode: string;
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  customers: Customer[];

  constructor(
    private http: HttpClient
  ) { }

  filterCustomerById(customerId: string, customers: Customer[]): Customer {
    return customers.filter((customer: Customer) => {
      return customer.id == parseInt(customerId);
    })[0];
  }

  getCustomers(): Observable<any> {
    const getUrl = `${baseUrl}/api/v1/customer_data.json`;

    return this.http.get(getUrl);
  }

  updateCustomer(customerId: string, params: Customer, callbackSuccess: Function) {
    const putUrl = `${baseUrl}/api/v1/customer_data/${customerId}`;

    this.http.put(putUrl, params).subscribe(
      data  => {
        console.log('PUT Request is successful ', data);
        callbackSuccess();
      },
      error  => {
        console.log('Error', error);
      }
    );
  }

  deleteCustomer(customerId: number, callbackSuccess: Function) {
    const deleteUrl = `${baseUrl}/api/v1/customer_data/${customerId}`;

    this.http.delete(deleteUrl).subscribe(
      data  => {
        console.log('PUT Request is successful ', data);
        callbackSuccess();
      },
      error  => {
        console.log('Error', error);
      }
    );
  }

  createCustomer(params: Customer, callbackSuccess: Function) {
    const createUrl = `${baseUrl}/api/v1/customer_data`;

    this.http.post(createUrl, params).subscribe(
      data  => {
        console.log('PUT Request is successful ', data);
        callbackSuccess();
      },
      error  => {
        console.log('Error', error);
      }
    );
  }
}
