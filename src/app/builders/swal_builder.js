import Swal from 'sweetalert2'

export default {
  build (title) {
    let Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    Toast.fire({
      type: 'success',
      title: title
    })

    Toast = null
  }
}