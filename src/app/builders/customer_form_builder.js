import { FormBuilder, Validators } from '@angular/forms';

const customerFields = {
  name: '',
  email: '',
  phone: '',
  address: ['', Validators.required],
  city: '',
  state: '',
  zipcode: '',
}

export default {
  build() {
    const formBuilder = new FormBuilder
    return formBuilder.group(customerFields)
  }
}