import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import customerFormBuilder from '../builders/customer_form_builder';
import { Router } from '@angular/router';
import SwalBuilder from '../builders/swal_builder';

@Component({
  selector: 'app-customer-new',
  templateUrl: './customer-new.component.html',
  styleUrls: ['./customer-new.component.scss']
})
export class CustomerNewComponent implements OnInit {
  customerForm;
  processingRequest = false;

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {
    this.customerForm = customerFormBuilder.build();
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.customerForm.valid) {
      this.processingRequest = true;
      this.customerService.createCustomer(this.customerForm.value, this.redirectToListAndEnableButton.bind(this));
    }
  }

  redirectToListAndEnableButton() {
    this.router.navigate(['/customers']);
    this.processingRequest = false;

    SwalBuilder.build('Customer created with success!');
  }
}
